extends CanvasLayer

#Tile items in the scene. Squares is the labels, squaretiles is the sprites
#below the labels.
var squares;
var squaretiles;

# Board struct array
var board

# In the C++ version, we had vertical and horizontal block size #defines
# and a boardsize variable for handling the total size of the board.
var BOARDSIZE;
var HORIZ;
var VERTICAL;

# We need to be able to change the tile textures as tiles are clicked.
# In order to do this, we need a couple ImageTextures. These globals will
# allow us to use these texures in any place in the code, allowing for init
# and clickhandler changes without making new texture objects.
var mutable_texture;
var selected_texture;

# Tracking variables for input. last_selected is an array containing the x,y
# coords of the last selected tile. had_last_selected is a tracker boolean to
# make sure we don't do stuff when no tile was previously selected. We won't
# try to set the texture of a tile that doesn't exist because the user never
# clicked one before starting the game.
var last_selected;
var had_last_selected;

# These are a couple tracker variables for the sound effects and music tracks
# so they can be changed and programmed in the script from anywhere.
var place_sound;
var music;

# Integer to hold the score for the puzzle. This generally starts at 100, see
# _ready() for information on what that actually is.
var score;

# on_Label_mouse_clicked(x,y)
#
# click handler for when a tile is clicked. This is set in fetchsquares()
# along with an auto-assignment of a hard-coded x,y that each tile uses.
# This means that so long as the array is fetched in the order of the squares,
# there was no need to hard-code x/y locations into the layout file, and yet
# we don't have to deal with mouse click location to figure this.
func on_Label_mouse_clicked(x,y):
	if had_last_selected:
		squaretiles[last_selected[0]][last_selected[1]].texture = mutable_texture;
	squaretiles[x][y].texture = selected_texture;
	last_selected = [x,y];
	had_last_selected = true;
	get_node("SelectSound").play();
	if(music.playing==false):
		music.play();

# fetchsquares()
#
# Initializes some globals we need to be able to change node labels
# on the fly in the code. It also sets some defaults I forgot when
# laying out the board for the first time. This saved time developing
# the game, and really has no other purpose. However, if it is removed
# and not updated in the layout file there will likely be issues with
# the code going forward.
func fetchsquares():
	squares = [
		[get_node("SSquare1/Label"),
			get_node("SSquare2/Label"),
			get_node("SSquare3/Label"),
			get_node("SSquare4/Label"),
			get_node("SSquare5/Label"),
			get_node("SSquare6/Label")],
		[get_node("SSquare7/Label"),
			get_node("SSquare8/Label"),
			get_node("SSquare9/Label"),
			get_node("SSquare10/Label"),
			get_node("SSquare11/Label"),
			get_node("SSquare12/Label")],
		[get_node("SSquare13/Label"),
			get_node("SSquare14/Label"),
			get_node("SSquare15/Label"),
			get_node("SSquare16/Label"),
			get_node("SSquare17/Label"),
			get_node("SSquare18/Label")],
		[get_node("SSquare19/Label"),
			get_node("SSquare20/Label"),
			get_node("SSquare21/Label"),
			get_node("SSquare22/Label"),
			get_node("SSquare23/Label"),
			get_node("SSquare24/Label")],
		[get_node("SSquare25/Label"),
			get_node("SSquare26/Label"),
			get_node("SSquare27/Label"),
			get_node("SSquare28/Label"),
			get_node("SSquare29/Label"),
			get_node("SSquare30/Label")],
		[get_node("SSquare31/Label"),
			get_node("SSquare32/Label"),
			get_node("SSquare33/Label"),
			get_node("SSquare34/Label"),
			get_node("SSquare35/Label"),
			get_node("SSquare36/Label")]];
	for row in range(0, 6):
		for column in range(0, 6):
			squares[row][column].connect("pressed", self, "on_Label_mouse_clicked", [row, column]);
			squares[row][column].mouse_filter=Control.MOUSE_FILTER_STOP;
			squares[row][column].mouse_default_cursor_shape=Control.CURSOR_POINTING_HAND;
			squares[row][column].rect_min_size = Vector2(100,100);
	squaretiles = [
		[get_node("SSquare1"),
			get_node("SSquare2"),
			get_node("SSquare3"),
			get_node("SSquare4"),
			get_node("SSquare5"),
			get_node("SSquare6")],
		[get_node("SSquare7"),
			get_node("SSquare8"),
			get_node("SSquare9"),
			get_node("SSquare10"),
			get_node("SSquare11"),
			get_node("SSquare12")],
		[get_node("SSquare13"),
			get_node("SSquare14"),
			get_node("SSquare15"),
			get_node("SSquare16"),
			get_node("SSquare17"),
			get_node("SSquare18")],
		[get_node("SSquare19"),
			get_node("SSquare20"),
			get_node("SSquare21"),
			get_node("SSquare22"),
			get_node("SSquare23"),
			get_node("SSquare24")],
		[get_node("SSquare25"),
			get_node("SSquare26"),
			get_node("SSquare27"),
			get_node("SSquare28"),
			get_node("SSquare29"),
			get_node("SSquare30")],
		[get_node("SSquare31"),
			get_node("SSquare32"),
			get_node("SSquare33"),
			get_node("SSquare34"),
			get_node("SSquare35"),
			get_node("SSquare36")]];

# initboard()
#
# Initializes the board variables, making sure that the values and failed
# values are all cleared so we can then generate a board.
func initboard():
	board = [
	[
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []}],
	[
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []}],
	[
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []}],
	[
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []}],
	[
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []}],
	[
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []},
		{value = 0, failed_values = []}]];

# squares_clear_lacecard()
#
# Built to clear the "lace card" or board filled with placeholder
# zeros or whatever else. This refers to old punched cards, where
# a "lace card" is when all holes of the card are punched. These are
# garbage cards and would usually jam the machine, or in this case
# they show superfluous zeros all over the screen. Just as meaningless
# as a lace card.
func squares_clear_lacecard():
	for row in squares:
		for tile in row:
			tile.set_text("");

# genboard()
#
# Jim's Venerable Kludge from Hell
#
# Generates the sudoku board. Unfortunately, translating this out of C
# made for some interesting hacks to make things work. Users will see the
# "easter egg" "FIASCO AVERSION CODE INITIATED!" when the board totally
# fails to generate and starts over because of some bad logical translations
# we didn't have time to iron out.
func genboard():
	var conflicts = true;
	var numberfail = false;
	var temp = 0;
	BOARDSIZE = 6;
	HORIZ = 2;
	VERTICAL = 3;
	
	var first_row_index = 0;
	var second_row_index = 0;
	var proposed_element = 0;
	# old first-row generator code from C++. This didn't translate well into gdscript
	# so it's cut for now.
#	while first_row_index < BOARDSIZE:
#		proposed_element = int(randi() % (BOARDSIZE))+1;
#		conflicts = true;
#		while conflicts == true:
#			conflicts = false;
#			second_row_index = 0;
#			while second_row_index < first_row_index:
#				if int(proposed_element) == int(board[0][second_row_index].value):
#					conflicts = true;
#					proposed_element+=1;
#					if proposed_element > BOARDSIZE:
#						proposed_element = 1;
#				second_row_index += 1;
#		board[0][first_row_index].value = proposed_element;
#		first_row_index += 1
#	printraw("First row fininshed\n")
	
	var row_index = 0;
	var column_index = 0;
	var attempted_value = 0;
	while row_index < BOARDSIZE:
		column_index = 0;
		while column_index < BOARDSIZE:
			randomize();
			if board[row_index][column_index].value == 0:
				board[row_index][column_index].value = (randi() % (BOARDSIZE))+1;
			conflicts = true;
			numberfail = false;
			# Represented by a do-while loop in C++, not possible in GDScript
			while conflicts == true and numberfail == false:
				conflicts = false;
				if board[row_index][column_index].value == 0:
					conflicts = true;
					board[row_index][column_index].value = 1;
					continue
				for failed_value_index in range(0, board[row_index][column_index].failed_values.size()):
					for failed_value_index2 in range(0, failed_value_index):
						if board[row_index][column_index].failed_values[failed_value_index] == board[row_index][column_index].failed_values[failed_value_index2]:
							board[row_index][column_index].failed_values.remove(failed_value_index);
					if board[row_index][column_index].value == board[row_index][column_index].failed_values[failed_value_index]:
						conflicts = true;
						board[row_index][column_index].value += 1;
						if board[row_index][column_index].value > BOARDSIZE:
							board[row_index][column_index].value = 1
						break;
				if conflicts:
					if(board[row_index][column_index].failed_values.size() >= BOARDSIZE):
						board[row_index][column_index].failed_values.empty();
#						printraw(str(board[row_index][column_index].value));
						if column_index <= 0:
							row_index -= 1;
							column_index = BOARDSIZE - 1;
						else:
							column_index -= 1;
						if row_index < 0:
							print("\nFIASCO AVERSION CODE INITIATED!");
							row_index = 0;
							column_index = -1;
							initboard();
							break;
						board[row_index][column_index].failed_values.append(
							board[row_index][column_index].value);
						if column_index <= 0:
							row_index -= 1;
							column_index = 5;
						else:
							column_index = column_index - 1;
						numberfail = true;
					continue;
				#Check for conflicts across the row
				var row_index_2 = 0;
				while row_index_2 < row_index:
					if(board[row_index][column_index].value == board[row_index_2][column_index].value):
						board[row_index][column_index].failed_values.append(
							board[row_index][column_index].value);
						board[row_index][column_index].value += 1;
						if(board[row_index][column_index].value > BOARDSIZE):
							board[row_index][column_index].value = 1;
						conflicts = true;
#						printraw(str(board[row_index][column_index].value));
						break;
					row_index_2 += 1;
				if conflicts:
					continue;
				
				#Check for conflicts across the column
				var column_index_2 = 0;
				while column_index_2 < column_index:
					if(board[row_index][column_index].value == board[row_index][column_index_2].value):
						board[row_index][column_index].failed_values.append(board[row_index][column_index].value)
						board[row_index][column_index].value += 1;
						if(board[row_index][column_index].value > BOARDSIZE):
							board[row_index][column_index].value = 1;
						conflicts = true;
#						printraw(str(board[row_index][column_index].value));
						break;
					column_index_2 += 1;
				if(conflicts):
					continue;
				
				# Check for conflicts in the block
				temp = int(column_index / VERTICAL + (row_index/HORIZ * HORIZ));
#				printraw(str(temp));
#				printraw(" ");
				row_index_2 = 0;
				while row_index_2 <= row_index:
					column_index_2 = 0;
					while column_index_2 < BOARDSIZE:
						if row_index_2 == row_index:
							if column_index_2 == column_index:
								break;
						if temp == int(column_index_2 / VERTICAL + (row_index_2/HORIZ * HORIZ)):
							if board[row_index][column_index].value == board[row_index_2][column_index_2].value:
								board[row_index][column_index].failed_values.append(board[row_index][column_index].value)
								board[row_index][column_index].value += 1;
								if(board[row_index][column_index].value > BOARDSIZE):
									board[row_index][column_index].value = 1;
#								printraw(str(board[row_index][column_index].value));
								conflicts = true;
								break;
						column_index_2 += 1;
					if conflicts:
						break;
					row_index_2 += 1;
				if conflicts:
					continue;
			column_index += 1;
		row_index += 1;
#		print()

#_input(event)
#
# Built to handle input events. It's what handles when the user types a number
# between 1 and 6 on the keyboard. that's basically all it does at the moment.
# It requires that a cell be clicked on the board so it can add the number to
# the active cell.
func _input(event):
#	print(event.as_text());
	if had_last_selected:
		if(event.as_text() == "1" || event.as_text() == "Kp 1"):
			squares[last_selected[0]][last_selected[1]].text = "1";
			board[last_selected[0]][last_selected[1]].value = 1;
			if(!place_sound.playing):
				place_sound.play();
		if(event.as_text() == "2" || event.as_text() == "Kp 2"):
			squares[last_selected[0]][last_selected[1]].text = "2";
			board[last_selected[0]][last_selected[1]].value = 2;
			if(!place_sound.playing):
				place_sound.play();
		if(event.as_text() == "3" || event.as_text() == "Kp 3"):
			squares[last_selected[0]][last_selected[1]].text = "3";
			board[last_selected[0]][last_selected[1]].value = 3;
			if(!place_sound.playing):
				place_sound.play();
		if(event.as_text() == "4" || event.as_text() == "Kp 4"):
			squares[last_selected[0]][last_selected[1]].text = "4";
			board[last_selected[0]][last_selected[1]].value = 4;
			if(!place_sound.playing):
				place_sound.play();
		if(event.as_text() == "5" || event.as_text() == "Kp 5"):
			squares[last_selected[0]][last_selected[1]].text = "5";
			board[last_selected[0]][last_selected[1]].value = 5;
			if(!place_sound.playing):
				place_sound.play();
		if(event.as_text() == "6" || event.as_text() == "Kp 6"):
			squares[last_selected[0]][last_selected[1]].text = "6";
			board[last_selected[0]][last_selected[1]].value = 6;
			if(!place_sound.playing):
				place_sound.play();

# check_board()
#
# This code checks the board on the "check board" button being pressed.
# It does a checksum of row and column sums, and if they don't match based
# on the square of every number from 1-9 it tells you the board is wrong. It
# currently doesn't enforce square matchning, since that's rather expensive and
# row/column plus what it leaves on the board is sufficient to ensure that that
# must be correct for row/column to work anyhow.
func check_board():
	var check_sum = 0;
	var failed = false;
	for row in range(0, BOARDSIZE):
		check_sum = 0;
		for column in range(0, BOARDSIZE):
			check_sum += board[row][column].value * board[row][column].value;
		if(check_sum != 91):
#			print(check_sum)
			failed = true;
			break;
	if(failed):
		get_node("Gotit").text = "Not quite, try again!";
		score -= 7;
		get_node("FailedSound").play();
		return;
	for column in range(0, BOARDSIZE):
		check_sum = 0;
		for row in range(0, BOARDSIZE):
			check_sum += board[row][column].value * board[row][column].value;
		if(check_sum != 91):
#			print(check_sum)
			failed = true;
			break;
	if(failed):
		get_node("Gotit").text = "Not quite, try again!";
		score -=7;
		get_node("FailedSound").play();
		return;
	else:
		get_node("Gotit").text = "Well done!";
		get_node("CompleteSound").play();
		get_node("Awesomebutton").show();
		get_node("GradingTimer").stop();
		var totalskills = int(score / 10);
		var chance = randi() % totalskills;
		gamestate.get_ClientPlayer().updateStats({
			creativity = int((totalskills - chance) / 2),
			logic = int((totalskills - chance)/2),
			communication = 0,
			initiative = 0,
			humanity = 0,
			vocational = chance,
			unassigned = 0})

# _ready()
#
# Sets the score, gets all the squares and sets their intial properties, then
# starts fetching other nodes like sound effects. It then checks to make sure
# the board is valid or valid-adjacent and completely restarts if it isn't.
func _ready():
	score = 100;
	randomize()
	
	fetchsquares();
	initboard();
	
	place_sound = get_node("PlaceSound");
	music = get_node("SPKR-SudokuMusic");
	
	BOARDSIZE = 6;
	HORIZ = 2;
	VERTICAL = 3;
	
	var chance = 0;
	var lasttwo = 0;
	
	selected_texture = ImageTexture.new();
	selected_texture.load("res://puzzles/Sudoku/SudokuSquare-Selected.svg");
	selected_texture.set_size_override(Vector2(25,25));
	
	mutable_texture = ImageTexture.new();
	mutable_texture.load("res://puzzles/Sudoku/SudokuSquare-Mutable.svg");
	mutable_texture.set_size_override(Vector2(25,25));
	
	last_selected = [0,0];
	had_last_selected = false;
	
	squares_clear_lacecard()
	var failedboard = true;
	while failedboard:
		failedboard = false;
		genboard()
#		for index_row in range(0, BOARDSIZE):
#			for index_column in range(0, BOARDSIZE):
#				printraw(str(board[index_row][index_column].value));
#				printraw(" ");
#			print();
		var rowsum = 0;
		var columnsum = 0;
		var blocksum_e = 0;
		var blocksum_o = 0;
		for index_row in range(0, BOARDSIZE):
			for index_column in range(0, BOARDSIZE):
				columnsum += board[index_row][index_column].value;
#			print(columnsum);
			if columnsum != 21:
				failedboard = true;
				break;
			columnsum = 0;
		if(failedboard):
			continue;
		for index_column in range(0, BOARDSIZE):
			for index_row in range(0, BOARDSIZE):
				rowsum += board[index_row][index_column].value;
#			print(rowsum);
			if rowsum != 21:
				failedboard = true;
				break;
			rowsum = 0;
		if(failedboard):
			continue;
	for index_row in range(0, BOARDSIZE):
		for index_column in range(0, BOARDSIZE):
			chance = randi() % 5
			if(chance < 2 && lasttwo < 2):
				board[index_row][index_column].value = 0
				squaretiles[index_row][index_column].texture = mutable_texture;
				lasttwo += 1;
			else:
				lasttwo -= 1;
			if board[index_row][index_column].value != 0:
				squares[index_row][index_column].set_text(str(board[index_row][index_column].value))
				squares[index_row][index_column].disabled=true;
	get_node("GradingTimer").start();

# _on_Awesomebutton_pressed()
#
# Quits the game and returns to the scene.
func _on_Awesomebutton_pressed():
	var tree_root = get_node("/root/world");
	tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -1));

# _on_GradingTimer_timeout()
#
# Decreases the score when the grading timer times out. Resets itself
# so that score is based on the amount of time to solve the puzzle.
func _on_GradingTimer_timeout():
	score -= 6;
