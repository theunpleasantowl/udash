#include<vector>
#include<random>
using namespace std;

#ifndef BOARDSIZE
#define BOARDSIZE 9
#define HORIZ 3
#define VERTICAL 3
#endif

/*
  Custom Sudoku Engine
  Written 2018-03-13 to 2018-03-14 Jim Read
  
  This sudoku engine #include"sudokudisplay.cpp"was created to work with virtually any size board as defined by
  the preprocessor macros at the top of the file.

  Basic data structure:

  Each tile is sorted into a number of indicies. First, there is the block index.
  This indicates a block number. To illustrate:

  .___.___.___.
  | 1 | 2 | 3 |
  |   |   |   |
  |---+---+---|
  | 4 | 5 | 6 |
  |   |   |   |
  |---+---+---|
  | 7 | 8 | 9 |
  |   |   |   |
  +---+---+---+

  This block index is used to solve for collisions in a specified block. Since it
  doesn't tie to any one dimension of an array, this will likely be the hardest to
  do. In order to accomplish this, all we really need do is multiply each
  dimension by 2 or 3, so the starting point of block 1 would be [(block-1)*2]
  [(block-1)*3] where 'block' is 1, or something along those lines.

  Next is the row index. This should speak for itself. This is a row of indicies.
  .______.
  |123456|
  +------+

  And finally, there is the column index. Again, should speak for itself.
  ._.
  |1|
  |2|
  |3|
  |4|
  |5|
  |6|
  +-+

  Note that these indicies are not actually stored in the value of the tile, but
  used as the indicies of a two dimensional array and/or calculated.

  Outside of the indicies, there is a value for the tile, which should remain zero
  if the final value is not known. Any failed guesses for the tile should go into
  the vector 'failed_values'. The reason for using a vector in this case is it is
  very fast, and can insert like a stack while still not restricting access to
  values further down the list.
  
  In order to keep any part of our tileset from being zero on completion, we should
  have a test in the validation code which prevents just such a case.
*/
struct tile
{
  int value;
  vector<int> failed_values;
};

tile board[BOARDSIZE][BOARDSIZE];

void buildboard()
{
  /*
In order to generate a correct board, we need a starting point and some code to
generate real random numbers. There is no way to do this save for a hardware random
device. Anything short of that will produce the same or roughly the same sequence
of numbers every single time, leading to the same puzzle being generated over and
over again.
  */
  random_device generator;
  uniform_int_distribution<int> distribution;
  int startingnumber = distribution(generator) % BOARDSIZE + 1;
  /*
These two booleans will be used later in the code. The basics are, if there is a
number that ends up conflicting and needs to be re-guessed, the conflicts variable
will be set to true. Should there be no possible combination, the numberfail
variable will be set to true. This will cause the code to backtrack and set the last
calculated tile's current value to be a failed number until it gets a patter that 
works.
  */
  bool conflicts = false;
  bool numberfail = false;
  /*
At the moment temp is used primarily for calculating squares within the grid. It
is used so that there is an easy way to check whether two squares are in the same
quadrant. If so, their values need be checked against each other. Using temp means
we only need calculate the quadrant of the current tile once, then we can compare
it against tiles simply by doing one quadrant calculation.
  */
  int temp = 0;
  /*
And now we generate the first row. This is sort of the seed row, and it determines
what the rest of the board can look like. Generating this in a discrete step means
that less comparison logic need be used, since the only portion of the board that
can possibly exist at this stage is the first row.
  */
  for(
    int first_row_index = 0;
    first_row_index < BOARDSIZE;
    first_row_index++)
  {
    //Loop and run until no conflicts, basically
    do
    {
      conflicts = false;
      board[0][first_row_index].value = (distribution(generator) %BOARDSIZE)+1;
      //Read this counter as (second (row index)) not ((second row) index)
      for(
        int second_row_index = 0;
        second_row_index < first_row_index;
        second_row_index ++)
      {
        if(
          board[0][first_row_index].value ==
          board[0][second_row_index].value)
        {
          conflicts = true;
        }
      }
    }
    while(
      conflicts);
  }

  /*
With our initial row out of the way, the code gets a little tricker. We need to run
through every tile and check it against row, column, and block values that already
exist. This will inevitably involve a lot of guesswork and backtracking, as sudoku
is not something that can be "mathematically" solved.

From here we loop twice- once for row and once for column.
  */
  for(
    int row_index = 1;
    row_index < BOARDSIZE;
    row_index ++)
  {
    for(
      int column_index = 0;
      column_index < BOARDSIZE;
      column_index ++)
    {
      //Our intial guess at what kind of number we're going to use.
      board[row_index][column_index].value = (distribution(generator) %BOARDSIZE) + 1;
      //And then we check it.
      do
      {
        //We assume everything is A-OK then prove ourselves wrong later.
        conflicts = false;
        numberfail = false;
        //Obviously something is wrong if there's a zero in the cell.
        if(
          board[row_index][column_index].value == 0)
        {
          //easiest way to get a new value is just say this cell conflicts and move on.
          conflicts = true;
          /*
          In earlier code, this would happen at the beginning. Now it assumes that the
          checkers will handle this part, usually by incrementing values. If we just say
          that there's a conflict and don't do anything about it, we'll end up in an
          infinite loop reporting an error that never gets solved.
          
          Since this is a zero-value case, the line below never ran for some reason, so
          that's where we'll start.
          */
          board[row_index][column_index].value = (distribution(generator) %BOARDSIZE) + 1;
          continue;
        }
        /*
          This is the part of the code where we look for numbers that have already been
          proven wrong, so we don't end up re-checking ourselves forever.
        */
        for(
          int value_index = 0;
          value_index < board[row_index][column_index].failed_values.size();
          value_index ++)
        {
          for(
            int value_index_2 = 0;
            value_index_2 < value_index;
            value_index_2++)
          {
            //This statement triggers if we have a duplicate bad value. This
            //happens occasionally because not everything checks this successfully
            if(
              board[row_index][column_index].failed_values[value_index] ==
              board[row_index][column_index].failed_values[value_index_2])
            {
              board[row_index][column_index].failed_values.pop_back();
            }
          }
          //And once we've weeded out duplicates that we've found so far, we look
          //to see if our current guess is proven bogus.
          if(
            board[row_index][column_index].value ==
            board[row_index][column_index].failed_values[value_index])
          {
            conflicts = true;
            board[row_index][column_index].value++;
            //If we've gotten past the largest number, snap back to 1.
            if(
              board[row_index][column_index].value > BOARDSIZE)
            {
              board[row_index][column_index].value = 1;
            }
            break;
          }
        }
        //Out of the loop, now we handle the error. This is one-time stuff and so
        //we realistically don't want it in an inner loop.
        if(conflicts)
        {
          //This checks to see if we've checked everything by just looking at the
          //size of our fail vector. Since duplicates were removed in the last step,
          //we can assume that on a 9x9 board 9 fails means the number is bad and
          //with the current setup no answer can complete this tile. At this point
          //we need to go back.
          if(board[row_index][column_index].failed_values.size() >= BOARDSIZE)
          {
            board[row_index][column_index].failed_values.clear();
            if(
              column_index == 0)
            {
              row_index --;
              column_index = BOARDSIZE - 1;
            }
            else
            {
              column_index --;
            }
            board[row_index][column_index].failed_values.emplace_back(
              board[row_index][column_index].value);
            if(
              column_index == 0)
            {
              row_index --;
               column_index = BOARDSIZE - 1;
            }
            else
            {
              column_index --;
            }
            numberfail = true;
          }
          continue;
        }
        
        //Now we check for conflicts across the row.
        for(
          int row_index_2 = 0;
          row_index_2 < row_index;
          row_index_2 ++)
        {
          if(
            board[row_index][column_index].value ==
            board[row_index_2][column_index].value)
          {
            board[row_index][column_index].failed_values.push_back(
              board[row_index][column_index].value);
            board[row_index][column_index].value ++;
            if(
              board[row_index][column_index].value > BOARDSIZE)
            {
              board[row_index][column_index].value = 1;
            }
            conflicts = true;
            break;
          }
        }
        if(conflicts)
        {
          continue;
        }

        //Now we check for conflicts across the column.
        for(
          int column_index_2 = 0;
          column_index_2 < column_index;
          column_index_2 ++)
        {
          if(
            board[row_index][column_index].value ==
            board[row_index][column_index_2].value)
          {
            board[row_index][column_index].failed_values.push_back(
              board[row_index][column_index].value);
            board[row_index][column_index].value ++;
            if(
              board[row_index][column_index].value > BOARDSIZE)
            {
              board[row_index][column_index].value = 1;
            }
            conflicts = true;
            break;
          }
          if(conflicts) break;
        }
        if(conflicts)
        {
          continue;
        }

        /*
          Now we need to check for conflicts across the block we're currently
          in. To do this, we need a couple of calculated values. One block
          index for the current tile, and a block index for each tile we are
          comparing to. To prevent repeat calculations, we can get the value
          for the current tile now and just recalculate each tile as we go.
          This should provide a balance between memory accesses and repeat ALU
          work.
        */
        temp = 0;
        temp = column_index / VERTICAL + (row_index/HORIZ * HORIZ);
        for(
          int row_index_2 = 0;
          row_index_2 <= row_index;
          row_index_2 ++)
        {
          for(
            int column_index_2 = 0;
            column_index_2 < BOARDSIZE;
            column_index_2 ++)
          {
            if(
              row_index_2 == row_index)
              if(
                column_index_2 == column_index)
                break;
            if(
              temp == column_index_2 / VERTICAL + (row_index_2/HORIZ*HORIZ))
            {
              if(board[row_index][column_index].value ==
                 board[row_index_2][column_index_2].value)
              {
                board[row_index][column_index].failed_values.push_back(
                  board[row_index][column_index].value);
                board[row_index][column_index].value++;
                if(
                  board[row_index][column_index].value > BOARDSIZE)
                {
                  board[row_index][column_index].value = 1;
                }
                conflicts = true;
              }
            }
          }
          if(conflicts) break;
        }
        if(conflicts)
        {
          continue;
        }
      }
      while(
        conflicts &&
        !numberfail);
      //At this point in the loop, we have a successful, non-conflicting tile
      //value and can move on to the next one.
    }
  }
  //By this point we have a working board.
}
