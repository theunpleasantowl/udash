extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var HORIZSIZE = 9;
var VERTSIZE = 4;

var score = 895;
var clicks = 0;

var tiles;
var visitedtiles;

var hazards;
var numsurrounding=0;

var knocked_texture;
var cleared_texture;
var current_texture;
var potential_texture;
var cleardisabled_texture;
var disabled_texture;
var selected_texture;

func makehazards():
	hazards = [[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}]];
	var potential;
	var lasttwo = 0;
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			potential = randi() %10;
			if(potential < 2 && lasttwo < 2 && (row_index > 1 || column_index >1) && (row_index < VERTSIZE-2 || column_index < HORIZSIZE-2)):
				hazards[row_index][column_index].ishazard = true;
				#tiles[row_index][column_index].texture_disabled = knocked_texture;
				lasttwo += 1;
			else:
				lasttwo -= 1;

func gettiles():
	visitedtiles=[];
	tiles = [[
		get_node("Tile1"),
		get_node("Tile2"),
		get_node("Tile3"),
		get_node("Tile4"),
		get_node("Tile5"),
		get_node("Tile6"),
		get_node("Tile7"),
		get_node("Tile8"),
		get_node("Tile9")],
		[
		get_node("Tile10"),
		get_node("Tile11"),
		get_node("Tile12"),
		get_node("Tile13"),
		get_node("Tile14"),
		get_node("Tile15"),
		get_node("Tile16"),
		get_node("Tile17"),
		get_node("Tile18")],
		[
		get_node("Tile19"),
		get_node("Tile20"),
		get_node("Tile21"),
		get_node("Tile22"),
		get_node("Tile23"),
		get_node("Tile24"),
		get_node("Tile25"),
		get_node("Tile26"),
		get_node("Tile27")],
		[
		get_node("Tile28"),
		get_node("Tile29"),
		get_node("Tile30"),
		get_node("Tile31"),
		get_node("Tile32"),
		get_node("Tile33"),
		get_node("Tile34"),
		get_node("Tile35"),
		get_node("Tile36")]];

func disableall():
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			tiles[row_index][column_index].disabled=true;

func textureresetall():
	visitedtiles.clear();
	tiles[0][0].grab_focus();
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			tiles[row_index][column_index].texture_disabled = disabled_texture;
			tiles[row_index][column_index].texture_normal = potential_texture;
			tiles[row_index][column_index].texture_focused = selected_texture;
			if column_index != 0:
				tiles[row_index][column_index].focus_neighbour_left = tiles[row_index][column_index - 1].get_path();
				tiles[row_index][column_index - 1].focus_neighbour_right = tiles[row_index][column_index].get_path();
			if row_index != 0:
				tiles[row_index][column_index].focus_neighbour_top = tiles[row_index - 1][column_index].get_path();
				tiles[row_index - 1][column_index].focus_neighbour_bottom = tiles[row_index][column_index].get_path();

func clickedtile(rowindex, columnindex):
	get_node("Selected").play()
	visitedtiles.append([rowindex,columnindex]);
	clicks += 1
	if clicks < 21:
		score += 5;
	else:
		score -= 3;
	numsurrounding = 0;
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			if ((row_index >= rowindex - 1 && row_index <= rowindex + 1) && (column_index >= columnindex - 1 && column_index <= columnindex + 1)):
				tiles[row_index][column_index].disabled=false;
				if(hazards[row_index][column_index].ishazard == true):
					numsurrounding+=1
			else:
				tiles[row_index][column_index].disabled=true;
			if([row_index,column_index] in visitedtiles):
				tiles[row_index][column_index].texture_normal = cleared_texture;
				tiles[row_index][column_index].texture_disabled = cleared_texture;
	if(hazards[rowindex][columnindex].ishazard == true):
		tiles[rowindex][columnindex].texture_normal = knocked_texture;
		tiles[rowindex][columnindex].texture_disabled = knocked_texture;
		disableall();
		get_node("Tryagain").show();
		get_node("Fail").play();
		score -= 10;
	else:
		tiles[rowindex][columnindex].texture_normal = current_texture;
	if(rowindex == VERTSIZE -1 && columnindex == HORIZSIZE - 1):
		disableall();
		get_node("Continue").show();
		get_node("Success").play()
	get_node("Obstacles").text = str(numsurrounding);
	get_node("Score").text = str(score);

func restart():
	makehazards();
	textureresetall();
	clickedtile(0,0);
	get_node("Tryagain").hide();

func connectstuff():
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			tiles[row_index][column_index].connect("pressed", self, "clickedtile", [row_index,column_index]);
			pass;
	pass;

func _ready():
	randomize();
	
	knocked_texture = ImageTexture.new();
	knocked_texture.load("res://puzzles/Relentless/knocked_tile.svg");
	
	cleared_texture = ImageTexture.new();
	cleared_texture.load("res://puzzles/Relentless/cleared_tile.svg");
	
	current_texture = ImageTexture.new();
	current_texture.load("res://puzzles/Relentless/current_tile.svg");
	
	potential_texture = ImageTexture.new();
	potential_texture.load("res://puzzles/Relentless/potential_tile.svg");
	
	cleardisabled_texture = ImageTexture.new();
	cleardisabled_texture.load("res://puzzles/Relentless/cleared_tile_disabled.svg");
	
	disabled_texture = ImageTexture.new();
	disabled_texture.load("res://puzzles/Relentless/unexplored_tile.svg");
	
	selected_texture = ImageTexture.new();
	selected_texture.load("res://puzzles/Relentless/selected_tile.svg");
	
	gettiles();
	restart();
	connectstuff();
	
	tutorializer_next();

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func failtocampus():
#	get_node("/root/world/Skills").skills["SAT"] = 1000;
	tutorializer_text[3] = "Not so easy, is it?\n\nFeel free to come back and give it another try!";
	tutorializer_next();

func backtocampus():
#	get_node("/root/world/Skills").skills["SAT"] = (score / 10 * 6) + 1000;
	tutorializer_text[3] = "That's it! Look for the bleached looking book to play Relentless.\n\nYou can also find Relentless by clicking 'Retake SAT' at the skills\nscreen.";
	tutorializer_next();
	
var tutorializer_played = false;
var tutorializer_page = 0;
var tutorializer_text = [
	"Welcome to Relentless!\n\nIn this game you'll see a grid of squares. Most of them appear\nto be blank.",
	"Your objective is to try and cross from the top left corner\nof the board to the bottom right without running into anything.\n\nYou show up as blue, tiles you've visited show up as green, and tiles\nyou can click are purple.\n\nBut wait, there's a catch!",
	"You can only see which tiles you've visited and the number\nof obstacles that surround your position! You can't see where\nthose obstacles are!\n\nIn order to clear this board, you have to think logically\nand use relentless patience to figure out where it is safe\nto proceed!\n\n\n\nWhen you're ready to give it a try, press 'Next'.",
	"That's it! Look for the bleached looking book to play Relentless.\n\nYou can also find Relentless by clicking 'Retake SAT' at the skills\nscreen."];

func tutorializer_next():
	get_node("Tutorializer").show();
	get_node("Tutorializer/NextButton").grab_focus();
	if(tutorializer_page <= tutorializer_text.size()-1):
		if tutorializer_page == 3:
			if tutorializer_played == false:
				get_node("Tutorializer").hide();
				tiles[0][0].grab_focus();
				tutorializer_played = true;
				return;
		get_node("Tutorializer/Tutorializer-Text").text = tutorializer_text[tutorializer_page];
		tutorializer_played = false;
		tutorializer_page += 1;
	else:
		get_node("Tutorializer").hide();
		self.queue_free();

func tutorializer_previous():
	if(tutorializer_page > 1):
		get_node("Tutorializer/Tutorializer-Text").text = tutorializer_text[tutorializer_page-2];
		tutorializer_page -= 1