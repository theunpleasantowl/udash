extends CanvasLayer

# Variables referring to scene objects. All of these objects correspond to a
# label denoting a certain type of skill to be spent.
var creativity_label;
var logic_label;
var communication_label;
var initiative_label;
var humanities_label;
var vocational_label;
var unspent_label;

# This is the number of points available to spend in the dialog.
var pool = 0;

# And here we need to make a few things available globally to functions. Namely,
# we need to be able to set each category label.
func _ready():
	creativity_label    = get_node("Stats_Panel/HBoxContainer/StatsNum/Creativity");
	logic_label         = get_node("Stats_Panel/HBoxContainer/StatsNum/Logic");
	communication_label = get_node("Stats_Panel/HBoxContainer/StatsNum/Communication");
	initiative_label    = get_node("Stats_Panel/HBoxContainer/StatsNum/Initiative");
	humanities_label    = get_node("Stats_Panel/HBoxContainer/StatsNum/Humanity");
	vocational_label    = get_node("Stats_Panel/HBoxContainer/StatsNum/Vocational");
	unspent_label       = get_node("Stats_Panel/SkillPool/unspent");
	updatePool();

# This is a delta of skills to be applied to the player. Using this will allow
# us to cleanly update the skills definition in player, and in the mean time
# track skills distribution as the player decides their path.
var skills = {
	"creativity" 	: 0,
	"logic"			: 0,
	"communication"	: 0,
	"initiative"	: 0,
	"humanity"		: 0,
	"vocational"	: 0,
	"unassigned"	: 0,
}

# These next few functions are handlers for when skill points get spent.
func _on_ButCre_pressed():
	if pool > 0:
		skills["creativity"] += 1;
		creativity_label.set_text(String(skills["creativity"]));
		updatePool(-1);

func _on_ButLog_pressed():
	if pool > 0:
		skills["logic"] += 1;
		logic_label.set_text(String(skills["logic"]));
		updatePool(-1);

func _on_ButCom_pressed():
	if pool > 0:
		skills["communication"] += 1;
		communication_label.set_text(String(skills["communication"]));
		updatePool(-1);

func _on_ButIni_pressed():
	if pool > 0:
		skills["initiative"] += 1;
		initiative_label.set_text(String(skills["initiative"]));
		updatePool(-1);

func _on_ButHum_pressed():
	if pool > 0:
		skills["humanity"] += 1;
		humanities_label.set_text(String(skills["humanity"]));
		updatePool(-1);

func _on_ButVoc_pressed():
	if pool > 0:
		skills["vocational"] += 1;
		vocational_label.set_text(String(skills["vocational"]));
		updatePool(-1);

func updatePool(delta=0):
	pool += delta;
	unspent_label.set_text(String(pool));

func _on_Button_pressed():
	var id = str (get_tree().get_network_unique_id())	# Get User ID
	var player = get_node("/root/world/players").get_node(id)
	skills["unassigned"] = pool
	player.updateStats(skills)
	self.queue_free()