extends StaticBody2D

# Foreign
onready var id = str (get_tree().get_network_unique_id())
var player
var playerStats
# Object Primatives
var _name
var _animate
var _textBox
var _sprite
# Member Vars
var inRange = false
var reqMet = false

### NPC Parameters ###
### Requirements:	creativity, logic, communication, 
###				initiative, humanity , vocational
onready var requirementName = "Skills"
onready var requirementVal = 80
var portrait = "res://objects/NPCPortraits/Mary.png"
var dialog = "Congratulations, you are ready to graduate.\nAll the puzzles and NPC interactions will be disabled. However, you can still slow down the progress of other players by blocking entrances and paths.\nContinue?"


func _ready():
	_name = get_node("label")
	_name.set_text("Graduate")
	_animate = get_node("RequirementsBox/animate")
	_textBox = get_node("RequirementsBox/DialogBox/Text")
	_textBox.set_text(" Requires: "+String(requirementVal)+" points")
#	_sprite.set_texture("")

func _process(delta):
	if (Input.is_action_pressed("select_item") and inRange == true and reqMet == true):
		inRange = false
		loadEvent()

func loadEvent():
	var map = 	load("res://objects/questgiverdialog.tscn")
	map = map.instance()
	get_tree().get_root().get_node("world").add_child(map)
	map.set_diag(portrait, dialog, -1) # -1 triggers a condition in questgiverdialog that goes to graduate player screen instead of regular skill allocation screen.
#	var map = 	load("res://objects/questgiverdialog.tscn")
#	get_tree().change_scene("res://objects/questgiverdialog.tscn")


func _on_ActivateTrigger_body_entered(body):
	player = gamestate.get_ClientPlayer()
	var score = 0
	for n in player.getStats().values():	# Manually sum Skills earned
		score += n
	if score >= requirementVal:
		reqMet = true
	if (body.get_name() == id):
		inRange = true

func _on_ActivateTrigger_body_exited(body):
	if (body.get_name() == id):
		inRange = false


## Chat Bubble
func _on_ChatTrigger_body_entered(body):
	if (body.get_name() == id):
		_animate.play("fade_in")
func _on_ChatTrigger_body_exited(body):
	if (body.get_name() == id):
		_animate.play_backwards("fade_in")