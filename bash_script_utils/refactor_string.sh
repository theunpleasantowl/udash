echo "Enter a String to Search for, Ex. \"my_string = 6\":"
read search

echo "New String Name:"
read test

echo "Choose File-Types to Operate on:"
echo "1 - Any file with Extentions"
echo "2 - Godot Scripts (.gd)"
echo "Other - Specify your Own:" 
read option

case $option in
1)
  type="*.*"
  ;;
2)
  type="*.gd"
  ;;
*)
  type=$option
  ;;
esac

for f in $type
do
	sed -i -e "s/${search}/${test}/" "$f"
done