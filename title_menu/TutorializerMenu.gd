extends CanvasLayer

var puzzle;

func pressed():
	get_node("SelectSound").play();

func startmusic(node):
	if(node == puzzle):
		get_node("/root/MainMenu/Music").play();
		get_tree().disconnect("node_removed", self, "startmusic");

func startpuzzle(puzzlename):
	pressed();
	get_node("/root/MainMenu/Music").stop();
	get_tree().connect("node_removed", self, "startmusic");
	puzzle = load(puzzlename).instance();
	self.add_child(puzzle);
	
func starttutorial(puzzlename):
	pressed();
	puzzle = load(puzzlename).instance();
	self.add_child(puzzle);

func _on_Exitbutton_pressed():
	pressed();
	self.queue_free();

func _on_RhythmButton_pressed():
	startpuzzle("res://puzzles/rhythm_tutorial.tscn");

func _on_SudokuButton_pressed():
	startpuzzle("res://puzzles/Sudoku-Tutorial/SudokuTutorial.tscn");

func _on_Relentless_pressed():
	startpuzzle("res://puzzles/Relentless-tutorial/Relentless-tutorial.tscn");

func _on_StumpButton_pressed():
	startpuzzle("res://puzzles/Stump-tutorial/Stump-tutorial.tscn");

func _on_StartGameButton_pressed():
	starttutorial("res://title_menu/tutorial_startinggame.tscn");

func _on_SkillsButton_pressed():
	starttutorial("res://title_menu/tutorial_skills.tscn");

func _on_GameplayButton2_pressed():
	starttutorial("res://title_menu/tutorial_gameplay.tscn");

