extends CanvasLayer
onready	var world = get_node("/root/world")
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

#variables for storing item counts, counters, and constant values for easy refrence
var items = {
	"simple_meal_count" 	: 2,
	"quality_meal_count"	: 2,
	"energy_count" 			: 2
}
const SIMPLE_VALUE = {
 	"health" 		: 12,
	"sanity" 		: 5,
}
const QUALITY_VALUE = {
	"health"		: 20,
	"sanity"		: 17
}
const ENERGY_VALUE = {
	"health"		: 42,
	"sanity"		: -20	
}
var _counter_simple
var _counter_quality
var _counter_energy
var _tooltip
var _modifiers
var _description
func _ready(): #on ready, load counters and reset them to current count
	_counter_simple = get_node("ViewportContainer/item_bar/simple_panel/simpleMeal/simpleCounter")
	_counter_quality = get_node("ViewportContainer/item_bar/quality_panel/qualityMeal/qualityCounter")
	_counter_energy = get_node("ViewportContainer/item_bar/energy_panel/energyDrink/energyCounter")
	_counter_simple.text = String(items["simple_meal_count"])
	_counter_quality.text = String(items["quality_meal_count"])
	_counter_energy.text = String(items["energy_count"])
	_tooltip = get_node("ViewportContainer/tooltip_panel")
	_modifiers = get_node("ViewportContainer/tooltip_panel/modifiers")
	_description = get_node("ViewportContainer/tooltip_panel/description")
	_tooltip.hide()
	pass

################# on_[button]_pressed() methods  #################
#	These methods define what happens when a button is pressed.
#	All methods check if the user has the item they're trying to use
#	if the user has the item it will decrease the number of items stored
#	and pass the appropriate value to the updateVitals() method in the parent node
###################################################################

func _on_simpleMeal_pressed():
#	print("clicked!")
	if(items["simple_meal_count"] > 0):
#		print("if entered")
		get_parent().updateVitals(SIMPLE_VALUE)
		items["simple_meal_count"] -= 1
		_counter_simple.text = String(items["simple_meal_count"])
func _on_qualityMeal_pressed():
#	print("clicked!")
	if(items["quality_meal_count"] > 0):
#		print("if entered")
		get_parent().updateVitals(QUALITY_VALUE)
		items["quality_meal_count"] -= 1
		_counter_quality.text = String(items["quality_meal_count"])
func _on_energyButton_pressed():
#	print("clicked!")
	if(items["energy_count"] > 0):
#		print("if entered")
		get_parent().updateVitals(ENERGY_VALUE)
		get_parent().update_buff(0.45)
		foodTimer(3,"energyDrinkOver") # Remove Multi after 3 secs
		items["energy_count"] -= 1
		_counter_energy.text = String(items["energy_count"])

onready var _timerBox = get_node("timerBox")
func energyDrinkOver():
	get_parent().update_buff(-0.45)
	
	#Remove Oldest Timer from Memory
	for i in _timerBox.get_child_count():
		_timerBox.get_child(i).queue_free()
		break
#	get_node(timer).queue_free()	#Need way to get child name
func foodTimer(time,effect):
	var timer = Timer.new()
	timer.set_wait_time(time)
	timer.connect("timeout", self, "energyDrinkOver")
	_timerBox.add_child(timer)
	timer.start()

################# add_[type] methods #################
# These methods are called from external nodes in order to modify the number of items stored
# the method updates the corresponding item and then updates the label associated with the item
# that will be displayed to the user
######################################################

func add_simple(amount):
	items["simple_meal_count"] += amount
	_counter_simple.text = String(items["simple_meal_count"])
	pass
func add_quality(amount):
	items["quality_meal_count"] += amount
	_counter_quality.text = String(items["quality_meal_count"])
	pass
func add_energy(amount):
	items["energy_count"] += amount
	_counter_energy.text = String(items["energy_count"])
	pass

################# _on_[buttonName]_mouse_entered() methods #################
# These methods are responsible for displaying the tooltips for the items when
# the mouse hovers over their respective buttons. Each method sets the text in the description
# and modifiers labels then makes the tooltip_panel appear
############################################################################

func _on_simpleMeal_mouse_entered():
	_modifiers.text = "Health +"+String(SIMPLE_VALUE["health"])+" sanity "+String(SIMPLE_VALUE["sanity"])
	_description.text = "Coffee: Cheap, instant boost.\n It will keep you energized. And happy?"
	_tooltip.show()
	pass # replace with function body
func _on_qualityMeal_mouse_entered():
	_modifiers.text = "Health +"+String(QUALITY_VALUE["health"])+" sanity +"+String(QUALITY_VALUE["sanity"])
	_description.text = "Pizza: Both delicious and 'nutritious'.\nThis slice will feed your body and soul."
	_tooltip.show()
	pass # replace with function body
func _on_energyDrink_mouse_entered():
	_modifiers.text = "Health +"+String(ENERGY_VALUE["health"])+" sanity "+String(ENERGY_VALUE["sanity"])
	_description.text = "Energy Drink: Massive boost to your physical ability.\nCosts some of your sanity."
	_tooltip.show()
	pass # replace with function body

################# _on_[buttonName]_mouse_exited() methods #################
# These methods hide the tooltip panel once the mouse stops hovering over
# the item buttons
###########################################################################

func _on_simpleMeal_mouse_exited():
	_tooltip.hide()
	pass # replace with function body
func _on_qualityMeal_mouse_exited():
	_tooltip.hide()
	pass # replace with function body
func _on_energyDrink_mouse_exited():
	_tooltip.hide()
	pass # replace with function body