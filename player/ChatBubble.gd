extends Control

# Object Primatives
var panel_size = Vector2()
var _label
var _bubbleBox
var _bubbleText
var _animate
var isBubbleShown = false
var timer
# Member Vars
var time = 4


func _ready():
	_label = get_node("label")
	_animate =		get_node("animate")
	_bubbleBox = 	get_node("DialogBox")
	_bubbleText = 	get_node("DialogBox/Text")
	_animate =		get_node("animate")
#	_bubbleBox.hide()	# Start Hidden
	# Bubble timer
	timer = Timer.new()
	timer.set_wait_time(time)
	timer.connect("timeout", self, "closeBubble")
	add_child(timer)	#Add to Scene
	pass

func openBubble(msg):
	_bubbleText.set_text(msg)
	_bubbleBox.set_size(_bubbleText.get_minimum_size())
#	_bubbleBox.show()
	if !isBubbleShown:
		_animate.play('fade_in')
		isBubbleShown = !isBubbleShown
	timer.start()
	
func closeBubble():
	if isBubbleShown:
		_animate.play_backwards('fade_in')
		isBubbleShown = !isBubbleShown